module.exports = (api) => {
    api.cache(true);

    return {
        presets: [
            '@babel/env',
            '@babel/preset-typescript',
            '@babel/react',
        ],
        plugins: [
            '@babel/plugin-transform-runtime',
            '@loadable/babel-plugin',
        ],
    };
};
