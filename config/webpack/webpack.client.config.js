import path from 'path';
import LoadablePlugin from '@loadable/webpack-plugin';

export const webpackClientConfig = {
    name: 'client',
    entry: {
        app: [path.resolve(__dirname, '../../app/index.tsx')],
    },
    output: {
        path: path.resolve(__dirname, '../../build/client'),
        publicPath: '/assets/',
        filename: '[name].js',
    },
    plugins: [new LoadablePlugin()],
};
