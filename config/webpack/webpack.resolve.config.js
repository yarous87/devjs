import path from 'path';

const getPath = (dir) => path.resolve(__dirname, '../../', dir);

export const webpackResolve = {
    resolve: {
        alias: {
            app: getPath('app'),
            hocs: getPath('app/hocs'),
            store: getPath('app/store'),
            components: getPath('app/components'),
            services: getPath('app/services'),
            modules: getPath('app/modules'),
            views: getPath('app/views'),
            routes: getPath('app/config/routes.js'),
            appConfig: getPath('app/config/appConfig.js'),
        },
        extensions: [
            '.jsx',
            '.js',
            '.json',
            '.ts',
            '.tsx',
        ],
    },
};
