/* eslint import/no-extraneous-dependencies: 0 */

import webpack from 'webpack';
import StyleLintPlugin from 'stylelint-webpack-plugin';

export const webpackDevelopmentConfig = {
    mode: 'development',
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.jsx?$|\.tsx?$/,
                exclude: /node_modules/,
                use: [
                    {
                        loader: 'eslint-loader',
                        options: {
                            failOnError: false,
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('development'),
        }),
        new StyleLintPlugin({
            failOnError: false,
        }),
    ],
};
