/* eslint import/no-extraneous-dependencies: 0 */

import NodeExternals from 'webpack-node-externals';
import path from 'path';

export const webpackServerConfig = {
    name: 'server',
    target: 'node',
    devtool: 'source-map',
    externals: [NodeExternals({
        allowlist: ['normalize.css'],
    })],
    node: {
        __filename: true,
        __dirname: true,
    },
    entry: {
        server: [path.resolve(__dirname, '../../server/index.tsx')],
    },
    output: {
        path: path.resolve(__dirname, '../../build/server'),
        filename: '[name].js',
    },
};
