import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import {
    ApolloClient, InMemoryCache, ApolloProvider, NormalizedCacheObject,
} from '@apollo/client';
import { loadableReady } from '@loadable/component';

import { App } from 'components/App/App.component';

import './index.scss';

declare global {
    interface Window {
        __APOLLO_STATE__: NormalizedCacheObject;
    }
}

const client = new ApolloClient({
    uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
    cache: new InMemoryCache().restore(window.__APOLLO_STATE__), // eslint-disable-line
});

loadableReady(() => {
    ReactDOM.hydrate(
        <ApolloProvider client={client}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </ApolloProvider>,
        document.querySelector('.root'),
    );
});
