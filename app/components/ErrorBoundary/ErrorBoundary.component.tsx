import React, { ErrorInfo } from 'react';

interface IErrorBoundaryProps {
    children: React.ReactNode,
}

interface IErrorBoundaryState {
    hasError: boolean,
    message: string,
}

export class ErrorBoundary extends React.Component<IErrorBoundaryProps, IErrorBoundaryState> {
    constructor(props: IErrorBoundaryProps) {
        super(props);
        this.state = { hasError: false, message: '' };
    }

    static getDerivedStateFromError(error: Error) {
        return { hasError: true, message: error.message };
    }

    componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        console.log(error, errorInfo);
    }

    render() {
        const { children } = this.props;
        const { hasError, message } = this.state;

        if (hasError) {
            return <h1>{message}</h1>;
        }

        return children;
    }
}
