import { gql } from '@apollo/client';

export const GET_POKEMON_DETAILS = gql`
    query pokemon($name: String!) {
        pokemon(name: $name) {
            id
            name
            sprites {
                front_default
                back_default
            }
            height
            weight
            game_indices {
                version {
                    name
                }
            }
            abilities {
                ability {
                    name
                }
            }
            forms {
                name
            }
        }
    }
`;

interface IPokemon {
    id: number,
    name: string,
    sprites: {
        [key: string]: string,
    },
    height: number,
    weight: number,
    game_indices: [{
        version: {
            name: string,
        }
    }],
    abilities: [{
        ability: {
            name: string,
        },
    }],
    forms: [{
        name: string,
    }]
}

export interface IPokemonDetailsResponse {
    pokemon: IPokemon
}
