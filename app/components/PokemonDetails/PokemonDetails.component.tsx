import React, { useMemo } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { Loading } from 'components/Loading/Loading.component';
import { GET_POKEMON_DETAILS, IPokemonDetailsResponse } from 'components/PokemonDetails/PokemonDetails.gql';
import { Helmet } from 'react-helmet';
import { routes } from '../../config/routes';

import './PokemonDetails.scss';

export default () => {
    // @ts-ignore
    const { name } = useParams();
    const { loading, error, data } = useQuery<IPokemonDetailsResponse>(GET_POKEMON_DETAILS, {
        variables: { name },
    });

    const images = useMemo(() => {
        const tmp = Object.values(data?.pokemon.sprites || {});
        tmp.shift();
        return tmp;
    }, [data]);

    if (loading) return <Loading />;

    if (error) throw new Error(error.message);

    if (!data?.pokemon.id) throw new Error('404');

    const { pokemon } = data;

    return (
        <>
            <Helmet>
                <title>{`Pokepedia - wszystko o ${pokemon.name}`}</title>
                <meta name="description" content={`Pokepedia - wszystko o ${pokemon.name}`} />
            </Helmet>

            <Link to={routes.root} className="App__Btn">
                Powrót do listy
            </Link>

            <div className="PokemonDetails">
                <h1>{pokemon.name}</h1>
                {images.map(((image) => <img key={image} src={image} alt={pokemon.name} />)) }
                <table>
                    <tbody>
                        <tr>
                            <th>Wzrost</th>
                            <td>{pokemon.height} cm</td>
                        </tr>
                        <tr>
                            <th>Waga</th>
                            <td>{pokemon.weight} kg</td>
                        </tr>
                        <tr>
                            <th>Gry</th>
                            <td>
                                {pokemon.game_indices.map((game) => game.version.name).join(', ')}
                            </td>
                        </tr>
                        <tr>
                            <th>Umiejętności</th>
                            <td>
                                {pokemon.abilities.map((ability) => ability.ability.name).join(', ')}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </>
    );
};
