import { gql } from '@apollo/client';

export const GET_POKEMONS_LIST = gql`
    query pokemons($limit: Int, $offset: Int) {
        pokemons(limit: $limit, offset: $offset) {
            count
            results {
                id
                name
                image
            }
        }
    }
`;

interface IPokemon {
    id: number,
    name: string,
    image: string,
}

export interface IPokemonsListResponse {
    pokemons: {
        count: number,
        results: IPokemon[],
    }
}
