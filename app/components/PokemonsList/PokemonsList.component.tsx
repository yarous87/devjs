import React from 'react';
import { Link } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import { Loading } from 'components/Loading/Loading.component';
import { GET_POKEMONS_LIST, IPokemonsListResponse } from 'components/PokemonsList/PokemonList.gql';
import { Helmet } from 'react-helmet';
import { routes } from '../../config/routes';
import { useQueryParams } from '../../hooks/useQueryParams';

import './PokemonList.scss';

const ITEMS_PER_PAGE = 12;

export default () => {
    const page = parseInt(
        useQueryParams().get('page') || '1',
        10,
    );
    const { loading, error, data } = useQuery<IPokemonsListResponse>(GET_POKEMONS_LIST, {
        variables: {
            limit: ITEMS_PER_PAGE,
            offset: ITEMS_PER_PAGE * (page - 1),
        },
    });

    if (loading) return <Loading />;

    if (error) throw new Error(error.message);

    if (!data?.pokemons.results.length) throw new Error('404');

    return (
        <>
            <Helmet>
                <title>{`Pokepedia - strona ${page}`}</title>
                <meta name="description" content={`Pokepedia - strona ${page}`} />
            </Helmet>
            <div className="PokemonList">
                {data.pokemons.results.map((pokemon) => (
                    <Link
                        to={routes.details.replace(':name', pokemon.name)}
                        key={pokemon.id}
                        className="PokemonList__Item"
                    >
                        <h3>{pokemon.name}</h3>
                        <img src={pokemon.image} alt={pokemon.name} />
                    </Link>
                ))}
            </div>

            <div className="PokemonList__Nav">
                {page > 1 && (
                    <Link to={`${routes.root}?page=${page - 1}`} className="App__Btn">
                        Poprzednia strona
                    </Link>
                )}
                {page * ITEMS_PER_PAGE < data.pokemons.count && (
                    <Link to={`${routes.root}?page=${page + 1}`} className="App__Btn">
                        Następna strona
                    </Link>
                )}
            </div>
        </>
    );
};
