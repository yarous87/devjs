import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { routes } from 'app/config/routes';
import { ErrorBoundary } from 'components/ErrorBoundary/ErrorBoundary.component';
import { PokemonsListView } from '../../views/PokemonsList.view';
import { PokemonDetailsView } from '../../views/PokemonDetails.view';

import 'normalize.css';
import './App.scss';

export const App: React.FC = () => (
    <div className="App">
        <ErrorBoundary>
            <Switch>
                <Route path={routes.root} component={PokemonsListView} exact />
                <Route path={routes.details} component={PokemonDetailsView} exact />
                <Route path="*" component={() => { throw new Error('404'); }} />
            </Switch>
        </ErrorBoundary>
    </div>
);
