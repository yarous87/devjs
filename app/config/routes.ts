const routes = {
    root: '/',
    details: '/pokemons/:name',
};

export {
    routes,
};
