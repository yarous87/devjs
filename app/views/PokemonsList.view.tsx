import React from 'react';
import loadable from '@loadable/component';
import { Loading } from 'components/Loading/Loading.component';

const PokemonsList = loadable(() => import('../components/PokemonsList/PokemonsList.component'), {
    fallback: <Loading />,
});

export const PokemonsListView: React.FC = () => (
    <PokemonsList fallback={<Loading />} />
);
