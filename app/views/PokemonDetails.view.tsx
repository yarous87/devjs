import React from 'react';
import loadable from '@loadable/component';
import { Loading } from 'components/Loading/Loading.component';

const PokemonDetails = loadable(() => import('../components/PokemonDetails/PokemonDetails.component'), {
    fallback: <Loading />,
});

export const PokemonDetailsView: React.FC = () => (
    <PokemonDetails />
);
