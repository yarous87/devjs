import merge from 'webpack-merge';
import { webpackCommonConfig } from './config/webpack/webpack.common.config';
import { webpackResolve } from './config/webpack/webpack.resolve.config';
import { webpackProductionConfig } from './config/webpack/webpack.production.config';
import { webpackDevelopmentConfig } from './config/webpack/webpack.development.config';
import { webpackClientConfig } from './config/webpack/webpack.client.config';
import { webpackServerConfig } from './config/webpack/webpack.server.config';

export default ({ env = 'prod', target = 'client' }) => {
    let config = merge(webpackCommonConfig, webpackResolve);

    if (env === 'prod') {
        config = merge(config, webpackProductionConfig);
    } else {
        config = merge(config, webpackDevelopmentConfig);
    }

    if (target === 'client') {
        config = merge(config, webpackClientConfig);
    } else {
        config = merge(config, webpackServerConfig);
    }

    return config;
};
