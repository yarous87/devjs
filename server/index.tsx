import 'source-map-support/register';
import path from 'path';
import express from 'express';
import React from 'react';
import fetch from 'cross-fetch';
import {
    ApolloClient, createHttpLink, InMemoryCache, ApolloProvider,
} from '@apollo/client';
import { renderToStringWithData } from '@apollo/client/react/ssr';
import { StaticRouter } from 'react-router-dom';
import { ChunkExtractor } from '@loadable/server';
import { Helmet } from 'react-helmet';
import { App } from 'components/App/App.component';

import template from './index.html';

const app = express();
const port = 3000;

if (process.env.NODE_ENV === 'development') {
    app.use('/assets', express.static('../client'));
}

app.get('*', async (req, res) => {
    try {
        const statsFile = path.resolve('../client/loadable-stats.json');
        const extractor = new ChunkExtractor({ statsFile, entrypoints: ['app'] });

        const client = new ApolloClient({
            ssrMode: true,
            link: createHttpLink({
                uri: 'https://graphql-pokeapi.vercel.app/api/graphql',
                credentials: 'same-origin',
                headers: {
                    cookie: req.header('Cookie'),
                },
                fetch,
            }),
            cache: new InMemoryCache(),
        });

        const jsx = extractor.collectChunks((
            <ApolloProvider client={client}>
                <StaticRouter location={req.url}>
                    <App />
                </StaticRouter>
            </ApolloProvider>
        ));

        const renderedApp = await renderToStringWithData(jsx);
        const helmet = Helmet.renderStatic();

        res.send(template
            .replace('{renderedApp}', renderedApp)
            .replace('{title}', helmet.title.toString())
            .replace('{meta}', helmet.meta.toString())
            .replace('{preload}', extractor.getLinkTags())
            .replace('{styles}', extractor.getStyleTags())
            .replace('{scripts}', extractor.getScriptTags())
            .replace(
                '{state}', JSON.stringify(client.extract()),
            ));
    } catch (e) {
        console.log(e);

        if (e.message === '404') {
            res.sendStatus(404);
            return;
        }

        res.sendStatus(500);
    }
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});
